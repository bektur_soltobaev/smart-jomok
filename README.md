# Smart Jomok web application

Web-service to diagnose psychiatric trauma of a child through tales and storytelling.

## Django Objects

Application has two apps - accounts and story. 
When sign up has two options: to sign up as adult or as a child. Later when login you get redirected to http://127.0.0.1:8000/story/ page,
where have three links to three types of tales: http://127.0.0.1:8000/story/incomplete/list/, http://127.0.0.1:8000/story/complete/list/ and http://127.0.0.1:8000/story/lab/list/

### Prerequisites

You need at least the following packages in your virtualenv:

- Django==3.0.6
- django-mptt==0.11.0
- django-widget-tweaks==1.4.8

### Installing

Create new virtual environment

```bash
$virtualenv venv
```

Activate virtual environment

```bash
$source path/to/venv//bin/activate
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requirements

```bash
pip install -r requirements.txt
```

### Make migrations and run server

```
$python manage.py makemigrations
$python manage.py migrate
$python manage.py runserver

```

## Deployment

Deployed site version can be found at https://smartjomok.herokuapp.com/




