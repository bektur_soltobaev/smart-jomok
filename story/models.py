from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel


User = get_user_model()


class TaleToComplete(models.Model):
    title = models.CharField(max_length=200, verbose_name='Тема')
    main_text = models.TextField(max_length=2000, verbose_name='Сказка')
    question = models.CharField(max_length=300, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('tale_to_complete_detail_url', kwargs={'id': self.id})

    def __str__(self):
        return self.title


class CompleteTale(MPTTModel):
    title = models.CharField(max_length=200, verbose_name='Тема')
    main_text = models.TextField(max_length=2000, verbose_name='Текст')
    question = models.CharField(max_length=1000, verbose_name='Вопрос')
    parent = TreeForeignKey('self', on_delete=models.CASCADE,  null=True, blank=True, related_name='child')

    def get_absolute_url(self):
        return reverse('complete_tale_detail_url', kwargs={'id': self.id})

    def __str__(self):
        return self.title


class Answer(models.Model):
    owner = models.ForeignKey(User, related_name='answer', on_delete=models.CASCADE, verbose_name='Автор')
    tale = models.ForeignKey('story.TaleToComplete', on_delete=models.CASCADE,
                             related_name='answer', blank=True, null=True)
    complete_tale = models.ForeignKey('story.CompleteTale', on_delete=models.CASCADE,
                                      related_name='complete_answer', blank=True, null=True)
    text = models.TextField(max_length=2000, verbose_name='Текст')

    def __str__(self):
        return self.owner.username

    class Meta:
        verbose_name = 'Answer'
        verbose_name_plural = 'Answers'


class LabTale(MPTTModel):
    title = models.CharField(max_length=200, verbose_name='Название сказки')
    main_text = models.TextField(max_length=2000, verbose_name='Текст')
    question = models.CharField(max_length=1000, verbose_name='Вопрос', blank=True, null=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='child')

    def get_absolute_url(self):
        return reverse('labtale_detail_url', kwargs={'id': self.id})

    def __str__(self):
        return self.title


class AnswerToShow(models.Model):
    tale = models.ForeignKey(LabTale, on_delete=models.CASCADE, related_name='answer_variants')
    text = models.TextField(max_length=2000, verbose_name='Текст')
    go_to = models.OneToOneField(LabTale, on_delete=models.CASCADE, related_name='answer_from')

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Answer to lab'
        verbose_name_plural = 'Answers to lab'
