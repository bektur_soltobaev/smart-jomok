from django import forms

from story.models import Answer


class AnswerCreateForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ('text', )
