from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from .models import TaleToComplete, CompleteTale, LabTale, Answer, AnswerToShow


class TaleToCompleteAdmin(admin.ModelAdmin):
    list_display = ['title']
    list_filter = ['title']
    list_display_links = ['title']
    search_fields = ['title']
    fields = ['title', 'main_text', 'question']


# class CompleteTaleAdmin(admin.ModelAdmin):
#     list_display = ['title', 'parent']
#     list_filter = ['title']
#     list_display_links = ['title']
#     search_fields = ['title']
#     fields = ['title', 'main_text', 'question', 'parent']

admin.site.register(
    CompleteTale,
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title'
    ),

)

admin.site.register(
    LabTale,
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title'
    ),

)

admin.site.register(Answer)
admin.site.register(AnswerToShow)

admin.site.register(TaleToComplete, TaleToCompleteAdmin)
# admin.site.register(CompleteTale, CompleteTaleAdmin)
