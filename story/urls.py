from django.urls import path

from story.views.tale_views import home, LabTaleView, LabTaleListView
from story.views.complete_tale_views import CompleteTaleListView, complete_tale_detail
from story.views.tale_to_complete_views import TaleToCompleteListView, tale_to_complete_detail


urlpatterns = [
    path('', home, name='index_url'),
    path('lab/<int:id>/', LabTaleView.as_view(), name='labtale_detail_url'),
    path('lab/list/', LabTaleListView.as_view(), name='labtale_list_url'),
    path('complete/<int:id>/', complete_tale_detail, name='complete_tale_detail_url'),
    path('complete/list/', CompleteTaleListView.as_view(), name='complete_tale_list_url'),
    path('incomplete/<int:id>/', tale_to_complete_detail, name='tale_to_complete_detail_url'),
    path('incomplete/list/', TaleToCompleteListView.as_view(), name='tale_to_complete_list_url')
]
