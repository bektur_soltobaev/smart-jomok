from django import views
from django.shortcuts import render, get_object_or_404

from story.models import CompleteTale
from story.forms import AnswerCreateForm


class CompleteTaleListView(views.View):

    def get(self, request):
        tales = CompleteTale.objects.filter(parent=None)
        return render(request, 'story/complete_tale_list.html', context={'tales': tales})


def complete_tale_detail(request, id):
    template_name = 'story/complete_tale.html'
    tale = get_object_or_404(CompleteTale, id=id)
    next_tale = CompleteTale.objects.get(parent=tale)
    if request.method == 'POST':
        answer_form = AnswerCreateForm(data=request.POST)
        if answer_form.is_valid():
            user = request.user
            # Create Answer object but don't save to database yet
            new_answer = answer_form.save(commit=False)
            # Assign the current tale to the answer
            new_answer.complete_tale = tale
            new_answer.owner = user
            # Save the answer to the database
            new_answer.save()
    else:
        answer_form = AnswerCreateForm()

    return render(request, template_name, {'tale': tale,
                                           'next_tale': next_tale,
                                           'answer_form': answer_form})
