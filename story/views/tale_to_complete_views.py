from django import views
from django.shortcuts import render, get_object_or_404

from story.models import TaleToComplete
from story.forms import AnswerCreateForm


class TaleToCompleteListView(views.View):

    def get(self, request):
        tales = TaleToComplete.objects.all()
        return render(request, 'story/tale_to_complete_list.html', context={'tales': tales})


def tale_to_complete_detail(request, id):

    template_name = 'story/tale_to_complete.html'
    tale = get_object_or_404(TaleToComplete, id=id)

    if request.method == 'POST':
        answer_form = AnswerCreateForm(data=request.POST)
        if answer_form.is_valid():
            user = request.user
            # Create Answer object but don't save to database yet
            new_answer = answer_form.save(commit=False)
            # Assign the current tale to the answer
            new_answer.tale = tale
            new_answer.owner = user
            # Save the answer to the database
            new_answer.save()
    else:
        answer_form = AnswerCreateForm()

    return render(request, template_name, {'tale': tale,
                                           'answer_form': answer_form})
