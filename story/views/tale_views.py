from django.shortcuts import render
from django.views.generic import ListView

from story.models import TaleToComplete
from django import views
from django.shortcuts import render, get_object_or_404

from story.models import LabTale


def home(request):
    return render(request, 'story/index.html')


class LabTaleListView(views.View):

    def get(self, request):
        tales = LabTale.objects.filter(parent=None)
        return render(request, 'story/labtale_list.html', context={'tales': tales})


class LabTaleView(views.View):

    def get(self, request, id):
        tale = get_object_or_404(LabTale, id=id)
        answers = tale.answer_variants.all()
        return render(request, 'story/labtale_detail.html', context={'tale': tale,
                                                                     'answers': answers})
